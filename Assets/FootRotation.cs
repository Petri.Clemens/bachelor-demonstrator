﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FootRotation : MonoBehaviour {

		public GameObject FootRotationObject;
		public GameObject ARController;

		public Animator animatorMenu;
	  public GameObject Panel;
		public GameObject Phase1;

		public Slider Slider;

		public GameObject Andy;

		public float AndyAnimFrame;

		// Use this for initialization
		void Start () {
		animatorMenu = Panel.GetComponent<Animator>();
		}

		// Update is called once per frame
		void Update () {
			Andy = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
			AndyAnimFrame = Andy.GetComponent<Animation>() ["Take 001"].time;

			FootRotationObject = ARController.GetComponent<GetHelperObject>().FootRotation;

	    if (animatorMenu.GetBool("active"))
	    {
	        FootRotationObject.SetActive(true);
	    }
			else{
					FootRotationObject.SetActive(false);
			}

			if (!Phase1.activeSelf || Slider.value > 0f || AndyAnimFrame <= 5.9f || AndyAnimFrame >= 6.1f){
					FootRotationObject.SetActive(false);
			}
		}
	}
