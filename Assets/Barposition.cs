﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Barposition : MonoBehaviour {

	public GameObject BarpositionObject;
	public GameObject ARController;

	public Animator animatorMenu;
  public GameObject Panel;
	public GameObject Phase1;

	public Slider Slider;

	public GameObject Andy;

	public float AndyAnimFrame;

	// Use this for initialization
	void Start () {
	animatorMenu = Panel.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		Andy = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
		AndyAnimFrame = Andy.GetComponent<Animation>() ["Take 001"].time;

		BarpositionObject = ARController.GetComponent<GetHelperObject>().Barposition;

    if (animatorMenu.GetBool("active"))
    {
        BarpositionObject.SetActive(true);
    }
		else{
				BarpositionObject.SetActive(false);
		}

		if (!Phase1.activeSelf || Slider.value > 0f || AndyAnimFrame <= 5.9f || AndyAnimFrame >= 6.1f){
				BarpositionObject.SetActive(false);
		}
	}
}
