﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoopAnimationDeaktiviert : MonoBehaviour {

	public Button Loop_aktiviert;

	public Slider Slider;

	public Animation anim;

	public GameObject andyAnimationSpeed;
	public GameObject Loop_deaktiviertButton;
	public GameObject Loop_aktiviertButton;


	// Use this for initialization
	void Start () {

	Loop_aktiviert.onClick.AddListener(TaskOnClick);

	}

	void TaskOnClick () {

		Slider.value = 1;

		Loop_deaktiviertButton.SetActive(true);
		Loop_aktiviertButton.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
		andyAnimationSpeed = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
		anim = andyAnimationSpeed.GetComponent<Animation> ();

        if (anim["Take 001"].time > 20.66667) {
            anim["Take 001"].time = 0;
        }
    }
}
