﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Unfold : MonoBehaviour {

    public Button ExpMenuButton;
    public Button PanelButton;

    public GameObject Panel;
    public GameObject Triangle;
    public GameObject CollapseObject;
    public GameObject Phase;

    public Animator animatorMenu;
    public Animator animatorTri;
    public Animator animatorCollapse;

    public bool activeMenu;
    public bool activeTri;

    public static int clickCount;

    RectTransform rth;
    RectTransform rtr;
    public float PanelHeight;

    void Start () {

        ExpMenuButton.onClick.AddListener(UnfoldMenu);
        ExpMenuButton.onClick.AddListener(clickCounterMenu);
        PanelButton.onClick.AddListener(CollapseMenu);
        PanelButton.onClick.AddListener(clickCounterPanel);

        animatorMenu = Panel.GetComponent<Animator>();
        activeMenu = animatorMenu.GetBool("active");

        animatorTri = Triangle.GetComponent<Animator>();
        activeTri = animatorTri.GetBool("active");

        animatorCollapse = CollapseObject.GetComponent<Animator>();

        clickCount = 0;

        rth = Panel.GetComponent<RectTransform>();
        rtr = Triangle.GetComponent<RectTransform>();
    }

    void clickCounterMenu()
    {
        if(clickCount == 0)
        {
            animatorCollapse.SetBool("active", false);
        }
    }

    void clickCounterPanel()
    {
        if (clickCount == 0)
        {
            animatorCollapse.SetBool("active", false);
        }
    }

    void UnfoldMenu() {

        if (animatorMenu.GetBool("active"))
        {
            animatorMenu.SetBool("active", false);
            animatorTri.SetBool("active", false);
            clickCount--;
        }

        else
        {
            animatorMenu.SetBool("active", true);
            animatorTri.SetBool("active", true);
            animatorCollapse.SetBool("active", true);
            clickCount++;
        }
    }

    public void CollapseMenu()
    {
        animatorTri = Triangle.GetComponent<Animator>();
        animatorMenu = Panel.GetComponent<Animator>();
        animatorCollapse = CollapseObject.GetComponent<Animator>();

        if (!animatorMenu.GetBool("active") && !animatorTri.GetBool("active") && animatorCollapse.GetBool("active") && Phase.activeSelf)
        {
            animatorMenu.SetBool("active", true);
            animatorTri.SetBool("active", true);
            clickCount++;
        }

        else if (animatorMenu.GetBool("active") && animatorTri.GetBool("active") && Phase.activeSelf)
        {
            animatorMenu.SetBool("active", false);
            animatorTri.SetBool("active", false);
            clickCount--;
        }
    }

    // Diese Funktion findet Verwendung in "PhasenController.cs"
    // Animation zurücksetzen

    public void ResetAnimation()
    {
        animatorMenu = Panel.GetComponent<Animator>();
        animatorTri = Triangle.GetComponent<Animator>();

        rth = Panel.GetComponent<RectTransform>();
        rtr = Triangle.GetComponent<RectTransform>();

        animatorMenu.enabled = false;
        animatorTri.enabled = false;

        rth.sizeDelta = new Vector2(rth.sizeDelta.x, 0);
        rtr.rotation = Quaternion.Euler(0,0,0);
    }

    public void EnableAnimation()
    {
        animatorMenu = Panel.GetComponent<Animator>();
        animatorTri = Triangle.GetComponent<Animator>();

        rth = Panel.GetComponent<RectTransform>();
        rtr = Triangle.GetComponent<RectTransform>();

        animatorMenu.enabled = true;
        animatorTri.enabled = true;
    }

    // Ckeck ob Irgendeines der Menüs gerade aktiv ist und aktiviere die Animation des Buttons, wenns ja
    // Wenn nicht, setz Animation des Buttons zurück
}
