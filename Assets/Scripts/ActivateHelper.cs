﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateHelper : MonoBehaviour {

	public Button TestButton;
	public GameObject TestObject;
	public GameObject Andy;

	// Use this for initialization
	void Start () {
		TestButton.onClick.AddListener(TestFunction);
	}

	void TestFunction(){

		Andy = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
		TestObject = Andy.transform.GetChild(1).gameObject;

		if(TestObject.activeSelf){
			TestObject.SetActive(false);
		}
		else{
			TestObject.SetActive(true);
		}

	}

	// Update is called once per frame
	void Update () {

	}
}
