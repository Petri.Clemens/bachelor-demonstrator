﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu_aktiviert : MonoBehaviour
{

    public Button Menu_false;
    public Button Menu_true;

    public GameObject Menu_false_Button;
    public GameObject Menu_true_Button;

    public GameObject Panel;
    public GameObject transparencyLayer;

    public Animator panelAnimator;
    public Animator transparencyAnimator;

    public bool isOpen;

    public Slider Slider;

    void Start()
    {
        Menu_true.onClick.AddListener(CloseInfo);
        panelAnimator = Panel.GetComponent<Animator>();
        transparencyAnimator = transparencyLayer.GetComponent<Animator>();
    }

    void CloseInfo()
    {
        Menu_false_Button.SetActive(true);
        Menu_true_Button.SetActive(false);

        isOpen = panelAnimator.GetBool("open");
        panelAnimator.SetBool("open", false);
        transparencyAnimator.SetBool("active", false);

        Slider.value = 1;
    }

}
