﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu_deaktiviert : MonoBehaviour {

    public Button Menu_false;
    public Button Menu_true;

    public GameObject Menu_false_Button;
    public GameObject Menu_true_Button;
    public GameObject PhasenController;

    public GameObject Panel;
    public GameObject transparencyLayer;

    public Animator panelAnimator;
    public Animator transparencyAnimator;

    void Start()
    {
        Menu_false.onClick.AddListener(OpenInfo);
        panelAnimator = Panel.GetComponent<Animator>();
        transparencyAnimator = transparencyLayer.GetComponent<Animator>();
    }

    void OpenInfo()
    {
        Menu_false_Button.SetActive(false);
        Menu_true_Button.SetActive(true);

        panelAnimator.SetBool("open", true);
        transparencyAnimator.SetBool("active", true);


        PhasenController.GetComponent<PhasenController>().P2Activation();
        PhasenController.GetComponent<PhasenController>().P1Activation();
    }

}
