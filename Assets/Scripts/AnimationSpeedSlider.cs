﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSpeedSlider : MonoBehaviour {

	public Animation anim;
	public GameObject andyObject;
	public float animSpeed;
	public Slider slider;
	
	public GameObject andyAnimationSpeed;
 
	void Start ()
	{
		anim = andyAnimationSpeed.GetComponent<Animation> ();
		anim.Play ("Take 001");
		animSpeed = 1;	
	}
	void Update ()
	{
		andyAnimationSpeed = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
		anim = andyAnimationSpeed.GetComponent<Animation> ();
		anim ["Take 001"].speed = animSpeed;
		animSpeed = slider.value;
	}
}
