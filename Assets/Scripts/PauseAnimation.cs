﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseAnimation : MonoBehaviour {

	public Button Play;
	public Button Pause;

	public GameObject PlayButton;
	public GameObject PauseButton;

	public Slider slider;

	// Use this for initialization
	void Start () {

	Pause.onClick.AddListener(TaskOnClick);

	}

	void TaskOnClick () {
		slider.value = 0f;
	}

	void Update() {
		if (slider.value == 0f) {
			PauseButton.SetActive(false);
			PlayButton.SetActive(true);
		}
	}

}