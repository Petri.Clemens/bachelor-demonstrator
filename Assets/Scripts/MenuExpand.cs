﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class MenuExpand : MonoBehaviour
{
    public GameObject Panel;
    RectTransform rt;
    public float PanelHeight;
    public float PanelMinHeight;

    // Animate the height of the GameObject
    public void Start()
    {
        Animation anim = GetComponent<Animation>();
        AnimationCurve curve;
        rt = (RectTransform)Panel.transform;
        PanelHeight = rt.rect.height;
        PanelMinHeight = 0;


        // create a new AnimationClip
        AnimationClip clip = new AnimationClip();
        clip.legacy = true;

        // create a curve to move the GameObject and assign to the clip
        Keyframe[] keys;
        keys = new Keyframe[2];
        keys[0] = new Keyframe(0.0f, PanelMinHeight);
        keys[1] = new Keyframe(20.0f, PanelHeight);
        curve = new AnimationCurve(keys);
        clip.SetCurve("", typeof(Transform), "height", curve);


        // now animate the GameObject
        clip.name = "ExpandMenu";
        anim.AddClip(clip, "ExpandMenu");
        anim.Play("ExpandMenu");
    }
}