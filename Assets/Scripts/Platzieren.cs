﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Platzieren : MonoBehaviour {

	public GameObject Andy;

	public GoogleARCore.Examples.HelloAR.HelloARController andyPlaced;

	public Button PlatzierenButton;

	public Slider Slider;


	void Start () 	{
		PlatzierenButton.onClick.AddListener(TaskOnClick);
	}

	void TaskOnClick () {
		Destroy(Andy);
		andyPlaced.m_isPlaced = false;
		Slider.value = 1f;		
	}
	
	void Update () {
		Andy = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
		andyPlaced = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>();
	}
}
