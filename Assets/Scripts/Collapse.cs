﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collapse : MonoBehaviour {

    public Animator CollapseAnimator;
    public Button CollapseButton;
    public GameObject CollapseObject;

    public GameObject MainPanel;
    
    public GameObject P1Button01;
    public GameObject P1Button02;
    public GameObject P1Button03;
    public GameObject P1Button04;
    public GameObject P1Button05;
    public GameObject P1Button06;
    
    public GameObject P2Button01;
    public GameObject P2Button02;
    public GameObject P2Button03;
    public GameObject P2Button04;
    public GameObject P2Button05;
    public GameObject P2Button06;
    public GameObject P2Button07;
    public GameObject P2Button08;
    public GameObject P2Button09;
    
    public GameObject P3Button01;
    public GameObject P3Button02;
    public GameObject P3Button03;
    public GameObject P3Button04;
    public GameObject P3Button05;
    
    public GameObject P4Button01;
    public GameObject P4Button02;
    public GameObject P4Button03;
    public GameObject P4Button04;
    
    public GameObject P5Button01;
    public GameObject P5Button02;
    public GameObject P5Button03;
    public GameObject P5Button04;
    public GameObject P5Button05;
       
    // Use this for initialization
    void Start () {

        CollapseButton.onClick.AddListener(CollapseAnimation);
        CollapseButton.onClick.AddListener(ExpandPhase1);
        CollapseButton.onClick.AddListener(ExpandPhase2);
        CollapseButton.onClick.AddListener(ExpandPhase3);
        CollapseButton.onClick.AddListener(ExpandPhase4);
        CollapseButton.onClick.AddListener(ExpandPhase5);
        CollapseAnimator = CollapseObject.GetComponent<Animator>();
	}
    
    void CollapseAnimation()
    {   if (CollapseAnimator.GetBool("active"))
        {
            CollapseAnimator.SetBool("active", false);
        }
        else
        {
            CollapseAnimator.SetBool("active", true);
        }
    }

    void ExpandPhase1()
    {
            P1Button01.GetComponent<Unfold>().CollapseMenu();
            P1Button02.GetComponent<Unfold>().CollapseMenu();
            P1Button03.GetComponent<Unfold>().CollapseMenu();
            P1Button04.GetComponent<Unfold>().CollapseMenu();
            P1Button05.GetComponent<Unfold>().CollapseMenu();
            P1Button06.GetComponent<Unfold>().CollapseMenu();
    }

    void ExpandPhase2()
    {
            P2Button01.GetComponent<Unfold>().CollapseMenu();
            P2Button02.GetComponent<Unfold>().CollapseMenu();
            P2Button03.GetComponent<Unfold>().CollapseMenu();
            P2Button04.GetComponent<Unfold>().CollapseMenu();
            P2Button05.GetComponent<Unfold>().CollapseMenu();
            P2Button06.GetComponent<Unfold>().CollapseMenu();
            P2Button07.GetComponent<Unfold>().CollapseMenu();
            P2Button08.GetComponent<Unfold>().CollapseMenu();
            P2Button09.GetComponent<Unfold>().CollapseMenu();
    }

    void ExpandPhase3()
    {
            P3Button01.GetComponent<Unfold>().CollapseMenu();
            P3Button02.GetComponent<Unfold>().CollapseMenu();
            P3Button03.GetComponent<Unfold>().CollapseMenu();
            P3Button04.GetComponent<Unfold>().CollapseMenu();
            P3Button05.GetComponent<Unfold>().CollapseMenu();
    }

    void ExpandPhase4()
    {
            P4Button01.GetComponent<Unfold>().CollapseMenu();
            P4Button02.GetComponent<Unfold>().CollapseMenu();
            P4Button03.GetComponent<Unfold>().CollapseMenu();
            P4Button04.GetComponent<Unfold>().CollapseMenu();
    }

    void ExpandPhase5()
    {
            P5Button01.GetComponent<Unfold>().CollapseMenu();
            P5Button02.GetComponent<Unfold>().CollapseMenu();
            P5Button03.GetComponent<Unfold>().CollapseMenu();
            P5Button04.GetComponent<Unfold>().CollapseMenu();
            P5Button05.GetComponent<Unfold>().CollapseMenu();
    }

}
