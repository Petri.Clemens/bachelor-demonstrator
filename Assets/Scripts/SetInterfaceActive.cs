﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetInterfaceActive : MonoBehaviour {

	public bool andyPlaced;
	public GameObject Canvas;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	andyPlaced = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().m_isPlaced;


		if (andyPlaced == false) {
			Canvas.SetActive(false);
		}

		else {
		Canvas.SetActive(true);
		}
		
	}
}
