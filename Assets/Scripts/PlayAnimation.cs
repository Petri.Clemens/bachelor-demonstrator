﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayAnimation : MonoBehaviour {

	public Button Play;
	public Button Pause;

	public GameObject PlayButton;
	public GameObject PauseButton;


	public Slider slider;

	// Use this for initialization
	void Start () {

	Play.onClick.AddListener(TaskOnClick);

	}

	void TaskOnClick () {
		slider.value = 1f;
	}

	void Update() {
		if (slider.value > 0f) {
			PlayButton.SetActive(false);
			PauseButton.SetActive(true);
		}
	}
}