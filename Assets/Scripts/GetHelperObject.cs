﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetHelperObject : MonoBehaviour {

	public GameObject Andy;

	public GameObject P1;

	public GameObject Barposition;
	public GameObject FootDistance;
	public GameObject FootFlat;
	public GameObject FootRotation;
	public GameObject Wirbelsaeule;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		Andy = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;

		P1 = Andy.transform.GetChild(6).gameObject;
		Barposition = P1.transform.GetChild(0).gameObject;
		FootDistance = P1.transform.GetChild(1).gameObject;
		FootFlat = P1.transform.GetChild(2).gameObject;
		FootRotation = P1.transform.GetChild(3).gameObject;
		Wirbelsaeule = P1.transform.GetChild(4).gameObject;

	}
}
