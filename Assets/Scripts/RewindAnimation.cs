﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewindAnimation : MonoBehaviour {

	public Button Rewind;

	public Slider Slider;

	public Animation anim;

	public float Frame;

	public GameObject andyAnimationSpeed;
	public GameObject Loop_deaktiviert;

	public LoopAnimation LoopReference;

	public float EndFrame;


	// Use this for initialization
	void Start () {

	Rewind.onClick.AddListener(RewindAnim);

	}

	public void RewindAnim () {

	andyAnimationSpeed = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;

	anim ["Take 001"].time = Frame;

		if (Loop_deaktiviert.activeInHierarchy == false) {
			LoopReference.StartBracket = Frame;
			LoopReference.EndBracket = EndFrame;

			anim ["Take 001"].time = Frame;

			Slider.value = 1;
		}

		else {
			Slider.value = 0;
		}

	}

	// Update is called once per frame
	void Update () {
		andyAnimationSpeed = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
		anim = andyAnimationSpeed.GetComponent<Animation> ();

		if(anim ["Take 001"].time > 2f && anim ["Take 001"].time <= 7.20f) {
			Frame = 0f;
			EndFrame = 5.20f;
		}

		if(anim ["Take 001"].time > 7.20f && anim ["Take 001"].time <= 14.09f) {
			Frame = 5.20f;
			EndFrame = 12.09f;
		}

		if(anim ["Take 001"].time > 14.09f && anim ["Take 001"].time <= 16.05f) {
			Frame = 12.09f;
			EndFrame = 14.05f;
		}

		if(anim ["Take 001"].time > 16.05f && anim ["Take 001"].time <= 18.00f) {
			Frame = 14.05f;
			EndFrame = 16.00f;
		}

		if(anim ["Take 001"].time > 18.00f && anim ["Take 001"].time <= 20.6f || anim ["Take 001"].time <= 2f) {
			Frame = 16.00f;
			EndFrame = 20.6f;
		}
	}
}
