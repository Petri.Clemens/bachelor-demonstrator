﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoopAnimation : MonoBehaviour {

	public Button Loop_deaktiviert;

	public Slider Slider;

	public Animation anim;

	public float Frame;
	public float StartFrame;
	public float EndFrame;
    public float StartBracket;
    public float EndBracket;

	public GameObject andyAnimationSpeed;
	public GameObject Loop_deaktiviertButton;
	public GameObject Loop_aktiviertButton;

  public Button P1;
  public Button P2;
  public Button P3;
  public Button P4;
  public Button P5;




	// Use this for initialization
	void Start () {

        StartBracket = 0f;
        EndBracket = 20.6667f;

	    Loop_deaktiviert.onClick.AddListener(LoopOnClick);

      P1.onClick.AddListener(P1SetLoop);
      P2.onClick.AddListener(P2SetLoop);
      P3.onClick.AddListener(P3SetLoop);
      P4.onClick.AddListener(P4SetLoop);
      P5.onClick.AddListener(P5SetLoop);

	    }

			public void P1SetLoop(){
				if(!Loop_deaktiviertButton.activeSelf)
				{
				StartBracket = 0f;
				EndBracket = 5.20f;
				}
			}

			public void P2SetLoop(){
				if(!Loop_deaktiviertButton.activeSelf)
				{
				StartBracket = 5.20f;
				EndBracket = 12.09f;
				}
			}

			public void P3SetLoop(){
				if(!Loop_deaktiviertButton.activeSelf)
				{
				StartBracket = 12.09f;
				EndBracket = 14.05f;
				}
			}

			public void P4SetLoop(){
				if(!Loop_deaktiviertButton.activeSelf)
				{
				StartBracket = 14.05f;
				EndBracket = 16.00f;
				}
			}

			public void P5SetLoop(){
				if(!Loop_deaktiviertButton.activeSelf)
				{
				StartBracket = 16.00f;
				EndBracket = 20.6f;
				}
			}

public void LoopOnClick () {

        StartBracket = StartFrame;
        EndBracket = EndFrame;

		Slider.value = 1;

        Loop_deaktiviertButton.SetActive(false);
        Loop_aktiviertButton.SetActive(true);


	}

	// Update is called once per frame
	void Update () {
		andyAnimationSpeed = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
		anim = andyAnimationSpeed.GetComponent<Animation> ();

		if(anim ["Take 001"].time > EndBracket){
			anim ["Take 001"].time = StartBracket;
		}


		if(anim ["Take 001"].time >= 0f && anim ["Take 001"].time <= 5.20f) {
			Frame = 0f;
			StartFrame = 0f;
			EndFrame = 5.20f;
		}

		if(anim ["Take 001"].time >= 5.20f && anim ["Take 001"].time <= 12.09f) {
			Frame = 5.20f;
			StartFrame =5.20f;
			EndFrame = 12.09f;
		}

		if(anim ["Take 001"].time >= 12.09f && anim ["Take 001"].time <= 14.05f) {
			Frame = 12.09f;
			StartFrame = 12.09f;
			EndFrame = 14.05f;
		}

		if(anim ["Take 001"].time >= 14.05f && anim ["Take 001"].time <= 16.00f) {
			Frame = 14.05f;
			StartFrame = 14.05f;
			EndFrame = 16.00f;
		}

		if(anim ["Take 001"].time >= 16.00f && anim ["Take 001"].time <= 20.6f) {
			Frame =16.00f;
			StartFrame = 16.00f;
			EndFrame = 20.6f;
		}


        if (Loop_deaktiviertButton.activeSelf == true)
        {
            StartBracket = 0f;
            EndBracket = 20.6667f;
        }
	}
}
