﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhasenController : MonoBehaviour {

    public Button P1;
    public Button P2;
    public Button P3;
    public Button P4;
    public Button P5;

    public GameObject Phase1;
    public GameObject P1Image;
    public GameObject P1Button01;
    public GameObject P1Button03;
    public GameObject P1Button04;
    public GameObject P1Button05;
    public GameObject P1Button06;

    public GameObject Phase2;
    public GameObject P2Image;
    public GameObject P2Button01;
    public GameObject P2Button02;
    public GameObject P2Button03;
    public GameObject P2Button04;
    public GameObject P2Button05;
    public GameObject P2Button06;
    public GameObject P2Button07;
    public GameObject P2Button08;
    public GameObject P2Button09;
    public GameObject P2Button10;

    public GameObject Phase3;
    public GameObject P3Image;
    public GameObject P3Button01;
    public GameObject P3Button02;
    public GameObject P3Button03;
    public GameObject P3Button04;
    public GameObject P3Button05;

    public GameObject Phase4;
    public GameObject P4Image;
    public GameObject P4Button01;
    public GameObject P4Button02;
    public GameObject P4Button03;
    public GameObject P4Button04;

    public GameObject Phase5;
    public GameObject P5Image;
    public GameObject P5Button01;
    public GameObject P5Button02;
    public GameObject P5Button03;
    public GameObject P5Button04;
    public GameObject P5Button05;

    public GameObject transparencyLayer;
    public Animator transparencyAnimator;

    public Animator CollapseAnimator;
    public GameObject CollapseObject;

    public Slider Slider;

    public Animation DemonstratorAnim;

    public GameObject Demonstrator;
    public GameObject Loop_deaktiviertButton;

    // Use this for initialization
    void Start () {

        P1.onClick.AddListener(P1Activation);
        P2.onClick.AddListener(P2Activation);
        P3.onClick.AddListener(P3Activation);
        P4.onClick.AddListener(P4Activation);
        P5.onClick.AddListener(P5Activation);

        Phase2.SetActive(false);
        Phase3.SetActive(false);
        Phase4.SetActive(false);
        Phase5.SetActive(false);

        transparencyAnimator = transparencyLayer.GetComponent<Animator>();

        CollapseAnimator = CollapseObject.GetComponent<Animator>();

    }

    void TempoReset(){
      if(!Loop_deaktiviertButton.activeSelf){
        Slider.value = 1;
      }
    }


    public void P1Activation()
    {

    Slider.value = 0;
    DemonstratorAnim["Take 001"].time = 6.00f;

        TempoReset();

        if (Phase1.activeSelf == false)
        {
            P1Button01.GetComponent<Unfold>().EnableAnimation();
            P1Button03.GetComponent<Unfold>().EnableAnimation();
            P1Button04.GetComponent<Unfold>().EnableAnimation();
            P1Button05.GetComponent<Unfold>().EnableAnimation();
            P1Button06.GetComponent<Unfold>().EnableAnimation();

            P2Button01.GetComponent<Unfold>().ResetAnimation();
            P2Button02.GetComponent<Unfold>().ResetAnimation();
            P2Button03.GetComponent<Unfold>().ResetAnimation();
            P2Button04.GetComponent<Unfold>().ResetAnimation();
            P2Button05.GetComponent<Unfold>().ResetAnimation();
            P2Button06.GetComponent<Unfold>().ResetAnimation();
            P2Button07.GetComponent<Unfold>().ResetAnimation();
            P2Button08.GetComponent<Unfold>().ResetAnimation();
            P2Button09.GetComponent<Unfold>().ResetAnimation();
            P2Button10.GetComponent<Unfold>().ResetAnimation();

            P3Button01.GetComponent<Unfold>().ResetAnimation();
            P3Button02.GetComponent<Unfold>().ResetAnimation();
            P3Button03.GetComponent<Unfold>().ResetAnimation();
            P3Button04.GetComponent<Unfold>().ResetAnimation();
            P3Button05.GetComponent<Unfold>().ResetAnimation();

            P4Button01.GetComponent<Unfold>().ResetAnimation();
            P4Button02.GetComponent<Unfold>().ResetAnimation();
            P4Button03.GetComponent<Unfold>().ResetAnimation();
            P4Button04.GetComponent<Unfold>().ResetAnimation();

            P5Button01.GetComponent<Unfold>().ResetAnimation();
            P5Button02.GetComponent<Unfold>().ResetAnimation();
            P5Button03.GetComponent<Unfold>().ResetAnimation();
            P5Button04.GetComponent<Unfold>().ResetAnimation();
            P5Button05.GetComponent<Unfold>().ResetAnimation();


            //          transparencyAnimator.SetTrigger("fadeTrigger");

            CollapseAnimator.SetBool("active", false);

            Phase1.SetActive(true);
            Phase2.SetActive(false);
            Phase3.SetActive(false);
            Phase4.SetActive(false);
            Phase5.SetActive(false);

            P1Image.SetActive(true);
            P2Image.SetActive(false);
            P3Image.SetActive(false);
            P4Image.SetActive(false);
            P5Image.SetActive(false);

        }
    }

  public void P2Activation()
    {

    Slider.value = 0;
    DemonstratorAnim["Take 001"].time = 11.10f;

        TempoReset();

        if (Phase2.activeSelf == false)
        {
            P1Button01.GetComponent<Unfold>().ResetAnimation();
            P1Button03.GetComponent<Unfold>().ResetAnimation();
            P1Button04.GetComponent<Unfold>().ResetAnimation();
            P1Button05.GetComponent<Unfold>().ResetAnimation();
            P1Button06.GetComponent<Unfold>().ResetAnimation();

            P2Button01.GetComponent<Unfold>().EnableAnimation();
            P2Button02.GetComponent<Unfold>().EnableAnimation();
            P2Button03.GetComponent<Unfold>().EnableAnimation();
            P2Button04.GetComponent<Unfold>().EnableAnimation();
            P2Button05.GetComponent<Unfold>().EnableAnimation();
            P2Button06.GetComponent<Unfold>().EnableAnimation();
            P2Button07.GetComponent<Unfold>().EnableAnimation();
            P2Button08.GetComponent<Unfold>().EnableAnimation();
            P2Button09.GetComponent<Unfold>().EnableAnimation();
            P2Button10.GetComponent<Unfold>().ResetAnimation();

            P3Button01.GetComponent<Unfold>().ResetAnimation();
            P3Button02.GetComponent<Unfold>().ResetAnimation();
            P3Button03.GetComponent<Unfold>().ResetAnimation();
            P3Button04.GetComponent<Unfold>().ResetAnimation();
            P3Button05.GetComponent<Unfold>().ResetAnimation();

            P4Button01.GetComponent<Unfold>().ResetAnimation();
            P4Button02.GetComponent<Unfold>().ResetAnimation();
            P4Button03.GetComponent<Unfold>().ResetAnimation();
            P4Button04.GetComponent<Unfold>().ResetAnimation();

            P5Button01.GetComponent<Unfold>().ResetAnimation();
            P5Button02.GetComponent<Unfold>().ResetAnimation();
            P5Button03.GetComponent<Unfold>().ResetAnimation();
            P5Button04.GetComponent<Unfold>().ResetAnimation();
            P5Button05.GetComponent<Unfold>().ResetAnimation();

            //          transparencyAnimator.SetTrigger("fadeTrigger");

            CollapseAnimator.SetBool("active", false);

            Phase1.SetActive(false);
            Phase2.SetActive(true);
            Phase3.SetActive(false);
            Phase4.SetActive(false);
            Phase5.SetActive(false);

            P1Image.SetActive(false);
            P2Image.SetActive(true);
            P3Image.SetActive(false);
            P4Image.SetActive(false);
            P5Image.SetActive(false);
        }
    }

    void P3Activation()
    {

    Slider.value = 0;
    DemonstratorAnim["Take 001"].time = 13.10f;

        TempoReset();


        if (Phase3.activeSelf == false)
        {
            P1Button01.GetComponent<Unfold>().ResetAnimation();
            P1Button03.GetComponent<Unfold>().ResetAnimation();
            P1Button04.GetComponent<Unfold>().ResetAnimation();
            P1Button05.GetComponent<Unfold>().ResetAnimation();
            P1Button06.GetComponent<Unfold>().ResetAnimation();

            P2Button01.GetComponent<Unfold>().ResetAnimation();
            P2Button02.GetComponent<Unfold>().ResetAnimation();
            P2Button03.GetComponent<Unfold>().ResetAnimation();
            P2Button04.GetComponent<Unfold>().ResetAnimation();
            P2Button05.GetComponent<Unfold>().ResetAnimation();
            P2Button06.GetComponent<Unfold>().ResetAnimation();
            P2Button07.GetComponent<Unfold>().ResetAnimation();
            P2Button08.GetComponent<Unfold>().ResetAnimation();
            P2Button09.GetComponent<Unfold>().ResetAnimation();
            P2Button10.GetComponent<Unfold>().ResetAnimation();

            P3Button01.GetComponent<Unfold>().EnableAnimation();
            P3Button02.GetComponent<Unfold>().EnableAnimation();
            P3Button03.GetComponent<Unfold>().EnableAnimation();
            P3Button04.GetComponent<Unfold>().EnableAnimation();
            P3Button05.GetComponent<Unfold>().EnableAnimation();

            P4Button01.GetComponent<Unfold>().ResetAnimation();
            P4Button02.GetComponent<Unfold>().ResetAnimation();
            P4Button03.GetComponent<Unfold>().ResetAnimation();
            P4Button04.GetComponent<Unfold>().ResetAnimation();

            P5Button01.GetComponent<Unfold>().ResetAnimation();
            P5Button02.GetComponent<Unfold>().ResetAnimation();
            P5Button03.GetComponent<Unfold>().ResetAnimation();
            P5Button04.GetComponent<Unfold>().ResetAnimation();
            P5Button05.GetComponent<Unfold>().ResetAnimation();

            //          transparencyAnimator.SetTrigger("fadeTrigger");

            CollapseAnimator.SetBool("active", false);

            Phase1.SetActive(false);
            Phase2.SetActive(false);
            Phase3.SetActive(true);
            Phase4.SetActive(false);
            Phase5.SetActive(false);

            P1Image.SetActive(false);
            P2Image.SetActive(false);
            P3Image.SetActive(true);
            P4Image.SetActive(false);
            P5Image.SetActive(false);
        }
    }


    void P4Activation()
    {

    Slider.value = 0;
    DemonstratorAnim["Take 001"].time = 15.50f;

        TempoReset();


        if (Phase4.activeSelf == false)
        {
            P1Button01.GetComponent<Unfold>().ResetAnimation();
            P1Button03.GetComponent<Unfold>().ResetAnimation();
            P1Button04.GetComponent<Unfold>().ResetAnimation();
            P1Button05.GetComponent<Unfold>().ResetAnimation();
            P1Button06.GetComponent<Unfold>().ResetAnimation();

            P2Button01.GetComponent<Unfold>().ResetAnimation();
            P2Button02.GetComponent<Unfold>().ResetAnimation();
            P2Button03.GetComponent<Unfold>().ResetAnimation();
            P2Button04.GetComponent<Unfold>().ResetAnimation();
            P2Button05.GetComponent<Unfold>().ResetAnimation();
            P2Button06.GetComponent<Unfold>().ResetAnimation();
            P2Button07.GetComponent<Unfold>().ResetAnimation();
            P2Button08.GetComponent<Unfold>().ResetAnimation();
            P2Button09.GetComponent<Unfold>().ResetAnimation();
            P2Button10.GetComponent<Unfold>().ResetAnimation();

            P3Button01.GetComponent<Unfold>().ResetAnimation();
            P3Button02.GetComponent<Unfold>().ResetAnimation();
            P3Button03.GetComponent<Unfold>().ResetAnimation();
            P3Button04.GetComponent<Unfold>().ResetAnimation();
            P3Button05.GetComponent<Unfold>().ResetAnimation();

            P4Button01.GetComponent<Unfold>().EnableAnimation();
            P4Button02.GetComponent<Unfold>().EnableAnimation();
            P4Button03.GetComponent<Unfold>().EnableAnimation();
            P4Button04.GetComponent<Unfold>().EnableAnimation();

            P5Button01.GetComponent<Unfold>().ResetAnimation();
            P5Button02.GetComponent<Unfold>().ResetAnimation();
            P5Button03.GetComponent<Unfold>().ResetAnimation();
            P5Button04.GetComponent<Unfold>().ResetAnimation();
            P5Button05.GetComponent<Unfold>().ResetAnimation();

            //          transparencyAnimator.SetTrigger("fadeTrigger");

            CollapseAnimator.SetBool("active", false);

            Phase1.SetActive(false);
            Phase2.SetActive(false);
            Phase3.SetActive(false);
            Phase4.SetActive(true);
            Phase5.SetActive(false);

            P1Image.SetActive(false);
            P2Image.SetActive(false);
            P3Image.SetActive(false);
            P4Image.SetActive(true);
            P5Image.SetActive(false);
        }
    }

    void P5Activation()
    {

            Slider.value = 0;
            DemonstratorAnim["Take 001"].time = 16.80f;

                TempoReset();


        if (Phase5.activeSelf == false)
        {
            P1Button01.GetComponent<Unfold>().ResetAnimation();
            P1Button03.GetComponent<Unfold>().ResetAnimation();
            P1Button04.GetComponent<Unfold>().ResetAnimation();
            P1Button05.GetComponent<Unfold>().ResetAnimation();
            P1Button06.GetComponent<Unfold>().ResetAnimation();

            P2Button01.GetComponent<Unfold>().ResetAnimation();
            P2Button02.GetComponent<Unfold>().ResetAnimation();
            P2Button03.GetComponent<Unfold>().ResetAnimation();
            P2Button04.GetComponent<Unfold>().ResetAnimation();
            P2Button05.GetComponent<Unfold>().ResetAnimation();
            P2Button06.GetComponent<Unfold>().ResetAnimation();
            P2Button07.GetComponent<Unfold>().ResetAnimation();
            P2Button08.GetComponent<Unfold>().ResetAnimation();
            P2Button09.GetComponent<Unfold>().ResetAnimation();
            P2Button10.GetComponent<Unfold>().ResetAnimation();

            P3Button01.GetComponent<Unfold>().ResetAnimation();
            P3Button02.GetComponent<Unfold>().ResetAnimation();
            P3Button03.GetComponent<Unfold>().ResetAnimation();
            P3Button04.GetComponent<Unfold>().ResetAnimation();
            P3Button05.GetComponent<Unfold>().ResetAnimation();

            P4Button01.GetComponent<Unfold>().ResetAnimation();
            P4Button02.GetComponent<Unfold>().ResetAnimation();
            P4Button03.GetComponent<Unfold>().ResetAnimation();
            P4Button04.GetComponent<Unfold>().ResetAnimation();

            P5Button01.GetComponent<Unfold>().EnableAnimation();
            P5Button02.GetComponent<Unfold>().EnableAnimation();
            P5Button03.GetComponent<Unfold>().EnableAnimation();
            P5Button04.GetComponent<Unfold>().EnableAnimation();
            P5Button05.GetComponent<Unfold>().EnableAnimation();

            //          transparencyAnimator.SetTrigger("fadeTrigger");

            CollapseAnimator.SetBool("active", false);

            Phase1.SetActive(false);
            Phase2.SetActive(false);
            Phase3.SetActive(false);
            Phase4.SetActive(false);
            Phase5.SetActive(true);


            P1Image.SetActive(false);
            P2Image.SetActive(false);
            P3Image.SetActive(false);
            P4Image.SetActive(false);
            P5Image.SetActive(true);
        }
    }

    void Update()
    {
        Demonstrator = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
        DemonstratorAnim = Demonstrator.GetComponent<Animation>();
    }
}
