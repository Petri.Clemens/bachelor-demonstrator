﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class StopAnimation : MonoBehaviour {

	public Animation anim;
	public Slider slider;
	
	public GameObject andyAnimationSpeed;

	public Button Stop;
 
	void Start ()	{
		Stop.onClick.AddListener(TaskOnClick);
	}

	void TaskOnClick () {
		slider.value = 0f;
		anim ["Take 001"].time = 0;
	}

	void Update () {
		andyAnimationSpeed = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
		anim = andyAnimationSpeed.GetComponent<Animation> ();
	}

}
