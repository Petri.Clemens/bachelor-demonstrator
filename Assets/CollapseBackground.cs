﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollapseBackground : MonoBehaviour {

		public Animator CollapseAnim;
		public Animator CollapseBackgroundAnim;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		if(CollapseAnim.GetBool("active")){
			CollapseBackgroundAnim.SetBool("active",true);
		}
		else{
			CollapseBackgroundAnim.SetBool("active",false);
		}

	}
}
