using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wirbelsaeule : MonoBehaviour {

	public GameObject ARController;

	public Animator animatorMenu;
  public GameObject Panel;
	public GameObject Phase1;

	public Slider Slider;

	public GameObject Andy;
	public GameObject Child;

	public float AndyAnimFrame;

	public Texture AndyTexture;
	public Texture WirbelsaeuleTextur;
	public Texture AndyDefaultTexture;

	public Renderer Renderer;

	// Use this for initialization
	void Start () {
	animatorMenu = Panel.GetComponent<Animator>();
	Renderer.material.EnableKeyword ("_MainTex");
	}

	// Update is called once per frame
	void Update () {
		Andy = GameObject.FindWithTag("Player").GetComponent<GoogleARCore.Examples.HelloAR.HelloARController>().andyObject;
		AndyAnimFrame = Andy.GetComponent<Animation>() ["Take 001"].time;
		Child = Andy.transform.GetChild(1).gameObject;
		Renderer = Child.GetComponent<Renderer>();
		AndyTexture = Renderer.material.mainTexture;

    if (animatorMenu.GetBool("active"))
    {
        AndyTexture = WirbelsaeuleTextur;
    }
		else{
				AndyTexture = AndyDefaultTexture;
		}

		if (!Phase1.activeSelf || Slider.value > 0f || AndyAnimFrame <= 5.9f || AndyAnimFrame >= 6.1f){
			AndyTexture = AndyDefaultTexture;
		}
	}
}
